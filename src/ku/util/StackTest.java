package ku.util;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
/**
 * StackTest Unit use to test 2 stack type.
 * @author Arut Thanomwatana
 *
 */
public class StackTest {

	/**First Stack*/
	Stack stack;
	/**Second Stack*/
	Stack<String> stack2;
	
	/**
	 * setUp is use for set the stack type and init 2 stack
	 */
	@Before
	public void setUp() {
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack(5);
		stack2 = StackFactory.makeStack(3);
		
	}

	/**
	 * Test whether if we create an new stack isEmpty method return true or not.
	 */
	@Test
	public void newStackIsEmpty(){
		assertTrue(stack.isEmpty());
	}
	
	/**
	 * Test whether if we push until its full isEmpty method return false or not.
	 */
	@Test
	public void testFullStackIsEmpty(){
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		assertFalse(stack.isEmpty());
	}
	
	/**
	 * Test wheter if we exceed push it,It shouldn't pop one that can't push.
	 */
	@Test
	public void testReplace(){
		stack2.push("A");
		stack2.push("B");
		stack2.push("C");
		stack2.push("D");
		assertEquals("D",stack2.pop());
		assertEquals("C",stack2.pop());
		assertEquals("B",stack2.pop());	
	}

	/** pop() should throw an exception if stack is empty */
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}
	
	/**
	 * Test whether we peek empty stack return null or not.
	 */
	@Test
	public void testPeekEmptyStack(){
		assertNull(stack.peek());
	}
	
	
	@Test (expected=java.lang.IllegalArgumentException.class)
	public void testNegativeCapacity(){
		stack = StackFactory.makeStack(-1);
	}

	@Test
	public void testCheckSizeWhenPeek(){
		stack2.push("test");
		assertEquals("test",stack2.peek());
		assertEquals(1,stack2.size());
		assertEquals("test",stack2.peek());
		assertEquals(1,stack2.size());
	}
	
	@Test (expected = java.lang.IllegalStateException.class)
	public void testOverPush(){
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
	}
	
	@Test
	public void testPopStack(){
		stack.push(1);
		stack.push(1);
		assertEquals(1,stack.pop());
	}
	@Test
	public void testStackIsEmpty(){
		assertEquals(5,stack.capacity());
		assertEquals(0,stack.size());
		assertTrue(stack.isEmpty());
	}
	
	@Test
	public void testCapacity(){
		assertEquals(5,stack.capacity());
	}
	
	@Test (expected = java.lang.IllegalArgumentException.class)
	public void testPushNull(){
		stack.push(null);
	}
	
	@Test
	public void StackIsFull(){
		assertFalse(stack2.isFull());
		stack2.push("1");
		stack2.push("1");
		stack2.push("1");
		assertTrue(stack2.isFull());
	}
	
	@Test
	public void testSize(){
		stack2.push("one");
		assertEquals(1,stack2.size());
		stack2.push("one");
		assertEquals(2,stack2.size());
		stack2.push("one");
		assertEquals(3,stack2.size());
	}

}

